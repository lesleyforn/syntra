<?php
/**
 * Created by PhpStorm.
 * User: Cuppens
 * Date: 20/12/2017
 * Time: 18:44
 */

?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="website.css"/>
    <title>Examen</title>
</head>
<body>
    <nav>
        <h1> LOGO </h1>
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Intro</a></li>
            <li><a href="#">Features</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </nav>
    <header>
        <h1>Dramatically Engage</h1>
        <h3>Objectively innovate empowered manufactured products whereas parallel platforms</h3>
        <button type="button">Engage Now</button>
    </header>
    <main>
        <section class="top">
            <h2> Superior Quality</h2>
            <p> Proactively envisioned multimedia based expertise and cross-media growth strategies.
                Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing.
                Holistically pontificate installed base portals after maintainable products without collateral.</p>
        </section>
        <section class="fotos">
            <h2 class="unl">Unleash</h2>
            <h2 class="syn">Synergize</h2>
            <h2 class="pro">Procrastinate</h2>
        </section>
        <section class="bottom">
            <img src="service.jpg" alt="service" />
            <h2> Contact Us </h2>
            <address> 31 Spooner street, Quahog, Rhode Island </address>
            <a href="tel:+32089354616">+32089354616</a>
            <a href="mailto:ifno@example.com?Subject=Hello%20again" >info@example.com</a>
        </section>
    <main>
    <footer>
        <h1> Newsletter</h1>
        <h2> Sign up for exclusive offers</h2>
        <input type="text" id="sub"/><button type="button" >Subscribe</button>
        <hr>
        <p> Copyright © Kobe Cuppens 20/12/2017 Dec 2017 </p>

    </footer>

</body>
</html>
