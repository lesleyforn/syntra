
<!DOCTYPE HTML>
<html>
<head>
  	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
	<title>Number Slots</title>
	<link rel="stylesheet" href="slots.css">
</head>
<body>
	<h1> 3D 3 number generator </h1>
	<div id="container">
		  <div id="draai">
			    <div id="wiel1" class="wiel"></div>
			    <div id="wiel2" class="wiel"></div>
			    <div id="wiel3" class="wiel"></div>
    	  </div>
  		  <div>
			    <button class="go">Spin</button>
		  </div>
		  <div>
			    <label>
				      <input type="checkbox" id="xray">
				      Toon binnenkant (zijkant view)
			    </label>
		  </div>
	</div>
	</script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
	<script type="text/javascript" src="slots.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.compatibility.js" ></script>
		
</body>
</html> 