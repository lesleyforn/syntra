const NrsPerWiel = 12;
const DraaiWiel = 150;

function createSlots (wiel) {
	
	var wielhoek = 360 / NrsPerWiel;

	var randNum = getrandNum();

	for (var i = 0; i < NrsPerWiel; i ++) {
		var slot = document.createElement('div');
		
		slot.className = 'slot';
		var transform = 'rotateX(' + (wielhoek * i) + 'deg) translateZ(' + DraaiWiel + 'px)';

		slot.style.transform = transform;

		// toon nr in box + genereer random nummer

		var content = $(slot).append('<p>' + ((randNum + i)%12)+ '</p>');
		wiel.append(slot);
	}
}

function getrandNum() {
	// genereer random number kleiner dan 13 daarna floor nr tussen 0 and inclusief 12
	return Math.floor(Math.random()*(NrsPerWiel));
}

function spin(timer) {
	for(var i = 1; i < 6; i ++) {
		var oldrandNum = -1;
		/*
		kijken als de old randNum niet hetzelfde is als deze, anders draaien de wielen niet
		*/
		var oldClass = $('#wiel'+i).attr('class');
		if(oldClass.length > 4) {
			oldrandNum = parseInt(oldClass.slice(10));
			console.log(oldrandNum);
		}
		var randNum = getrandNum();
		while(oldrandNum == randNum) {
			randNum = getrandNum();
		}

		$('#wiel'+i)
			.css('animation','back-spin 1s, spin-' + randNum + ' ' + (timer + i*0.5) + 's')
			.attr('class','wiel spin-' + randNum);
	}
}

$(document).ready(function() {

	// creer wielen
 	createSlots($('#wiel1'));
 	createSlots($('#wiel2'));
 	createSlots($('#wiel3'));
 

 	$('.go').on('click',function(){
 		var timer = 2;
 		spin(timer);
 	})
 });

$('#xray').on('click',function(){
 		//var isChecked = $('#xray:checked');
 		var tilt = 'tiltout';
 		
    if($(this).is(':checked')) {
 			tilt = 'tiltin';
 			$('.slot').addClass('backface-on');
 			$('#draai').css('animation',tilt + ' 2s 1');

			setTimeout(function(){
			  $('#draai').toggleClass('tilted');
			},2000);
 		} else {
      tilt = 'tiltout';
 			$('#draai').css({'animation':tilt + ' 2s 1'});

			setTimeout(function(){
	 			$('#draai').toggleClass('tilted');
	 			$('.slot').removeClass('backface-on');
	 		},1900);
 		}
 	})