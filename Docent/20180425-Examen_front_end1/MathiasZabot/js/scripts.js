/*var data = {"websites":
        [{
    "name": "ChannelFireball",
    "link": "http://store.channelfireball.com/landing",
    "mainTags": {
        "1":"competitive",
        "2": "standard",
        "3": "modern"}
    },{
            "name": "StarCityGames",
            "link": "http://www.starcitygames.com/",
            "mainTags":{
                "1":"competitive",
                "2": "standard",
                "3": "modern"}
    }]
};
var userInput;
var content;*/


$(document).ready(function () {

    var options = {
        valueNames: [ 'name', 'formats',  { attr: 'href', name: 'link' }, { attr: 'src', name: 'image' }, ],
        item: '<li><a href="" class="link name"></a></h3><p class="formats"></p><img class="image"></li>'
    };

    var values = [{
        link: 'http://store.channelfireball.com/landing',
        name: 'ChannelFireball',
        formats: ['competitive', 'standard', 'modern', 'legacy'],
        image: 'https://www.channelfireball.com/videos/'
    },
        {
            name: 'StarCityGames',
            formats: ['competitive', 'standard', 'modern', 'legacy']
        },
        {
            name: 'EDHrec',
            formats: ['commander', 'edh']
        }];

    var contentList = new List('mtgContent', options, values);

    contentList.add({
        name: "GatheringMagic",
        formats: ['competitive', 'casual', 'commander', 'edh']

    });

    $( ".options" ).accordion({
        active: false,
        collapsible: true,
        event: "mouseover"
    }).mouseout(function () {

    });

    $('.sort').on('click', function () {
        var button1 = $('#button1');
        if (this.id !== 'button1'){
            var button1html = button1.text();
            console.log(button1html);
            button1.text($(this).text());
            $(this).text(button1html);
        }
    });

    var offset = $('.list li').length;

    $('.list').endlessScroll({
        fireOnce: false,
        fireDelay: false,
        loader: '<div class=\"loading\"><div>',
        insertAfter: '.list li:last',
        callback: function () {
            
        } ,
        content: function(i) {
            return '<li>' + (i + offset) + '</li>';
        }
    });

    /*$('#searchField').on('keyup change', function () {
        if(event.keyCode === 13) {
            event.preventDefault();
            $('#searchButton').click();
        }else {
            getInput(this.value);
            console.log(this.value);
        }
    });

    $('#searchButton').click(function () {
        if (userInput.length) {
            searchContent();
        }*/
});

/*function getInput(value){
    userInput = value;
}

function searchContent() {
    $.each(data.websites, function (i, v) {
        if (v.name.toLowerCase() === userInput.toLowerCase() || v.mainTags["1"].toLowerCase() === userInput.toLowerCase() || v.mainTags["2"].toLowerCase() === userInput.toLowerCase() || v.mainTags["3"].toLowerCase() === userInput.toLowerCase()) {
            $('.mainContent').html(v.name);
            console.log('test');
        }
    })
}

function ListConstructor (content){
    this.content = content;
}*/


