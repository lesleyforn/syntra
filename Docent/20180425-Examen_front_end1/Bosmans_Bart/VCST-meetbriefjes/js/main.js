var maat;
var plusTolerantie;
var minTolerantie;
var instelMaat;
var richting;
var instellenOp;
var bereikKlok;
var verdeling;
var totaleBreedte;
var breedteVanDeBalk;


$(document).ready(function () {

    $("#submit").on('click', function () {

        // variabelen invoerveld formulier
        maat = $("#Maat").val();
        plusTolerantie = $("#plusTol").val();
        minTolerantie = $("#minTol").val();
        instelMaat = $("#instelmaat").val();
        richting = $("richting").val();
        instellenOp = $("#instellenOp").val();
        bereikKlok = $("#bereikMeetklok").val();
        verdeling = $("#verdeling").val();

        // variabele zelf aan te passen
        totaleBreedte = 100;
        breedteVanDeBalk = 6;           // factor voor de breedte van de balk aan te passen
        richting = true;

        // hulpvariabelen
        var breedte = totaleBreedte * breedteVanDeBalk;
        var verhouding = breedte / bereikKlok;
        var verschovenNulpunt = +maat - instelMaat - (instellenOp / 1000);
        var minTolerantieMm = minTolerantie / 1000;

        var buitenMinTolMm = (bereikKlok / 2) + (minTolerantie / 1000) + verschovenNulpunt;
        var buitenMinTolPx = Number(buitenMinTolMm * verhouding).toFixed(0);
        var binnenMinTolMm = -((minTolerantie / 1000) + verschovenNulpunt);
        var binnenMinTolPx = Number(binnenMinTolMm * verhouding).toFixed(0);
        var binnenPlusTolMm = (plusTolerantie / 1000) + verschovenNulpunt;
        var binnenPlusTolPx = Number(binnenPlusTolMm * verhouding).toFixed(0);
        var buitenPlusTolMm = (bereikKlok / 2) - (plusTolerantie / 1000) - verschovenNulpunt;
        var buitenPlusTolPx = Number(buitenPlusTolMm * verhouding).toFixed(0);


        var buitenMinTolTextRel = buitenMinTolPx - 20;
        var minLeeg = (breedte / 2) - (buitenMinTolTextRel + 50);
        var nulpuntTextRel = 20;
        var buitenPlusTolTextRel = buitenPlusTolPx - 20;
        var plusLeeg = (breedte / 2) - (buitenPlusTolTextRel + 50);

        //Functies opbouw balk

        //Balken maximum breedte bars


        function bars() {
            var start = document.getElementById('bars');
            start.style.width = +breedte + 'px';
        }

        //Balk -> Bar
        function OutMinTol() {
            var start1 = document.getElementById('OutMinTolerance');
            var start2 = document.getElementById('OutMinTolerance2');
            start1.style.width = buitenMinTolPx + 'px';
            start2.style.width = buitenMinTolPx + 'px';
        }

        function InMinTol() {
            var start1 = document.getElementById('InMinTolerance');
            var start2 = document.getElementById('InMinTolerance2');
            start1.style.width = binnenMinTolPx + 'px';
            start2.style.width = binnenMinTolPx + 'px';
        }

        function InPlusTol() {
            var start1 = document.getElementById('InPlusTolerance');
            var start2 = document.getElementById('InPlusTolerance2');
            start1.style.width = binnenPlusTolPx + 'px';
            start2.style.width = binnenPlusTolPx + 'px';
        }

        function OutPlusTol() {
            var start1 = document.getElementById('OutPlusTolerance');
            var start2 = document.getElementById('OutPlusTolerance2');
            start1.style.width = buitenPlusTolPx + 'px';
            start2.style.width = buitenPlusTolPx + 'px';
        }

        //Balk ->bovenBar

        function RelOutMinTol() {
            var start1 = document.getElementById('relOutMinToleranceText');
            var start2 = document.getElementById('relOutMinToleranceText2');
            start1.style.width = buitenMinTolTextRel + 'px';
            start2.style.width = buitenMinTolTextRel + 'px';
        }

        function RelMinTol() {
            var start1 = document.getElementById('relTextMinTol');
            var start2 = document.getElementById('relTextMinTol2');
            start1.style.width = 40 + 'px';
            start2.style.width = 40 + 'px';
            relTextMinTol.innerHTML = Number(-binnenMinTolMm * verdeling).toFixed(0);
            relTextMinTol2.innerHTML = Number(-binnenMinTolMm * verdeling).toFixed(0);
        }

        function RelLeegMin() {
            var start1 = document.getElementById('relMinLeeg');
            var start2 = document.getElementById('relMinLeeg2');
            start1.style.width = minLeeg + 'px';
            start2.style.width = minLeeg + 'px';
        }

        function RelNul() {
            var start1 = document.getElementById('relTextNulpunt');
            var start2 = document.getElementById('relTextNulpunt2');
            start1.style.width = nulpuntTextRel + 'px';
            start2.style.width = nulpuntTextRel + 'px';
        }

        function RelLeegPlus() {
            var start1 = document.getElementById('relPlusLeeg');
            var start2 = document.getElementById('relPlusLeeg2');
            start1.style.width = plusLeeg + 'px';
            start2.style.width = plusLeeg + 'px';
        }

        function RelPlusTol() {
            var start1 = document.getElementById('relTextPlusTol');
            var start2 = document.getElementById('relTextPlusTol2');
            start1.style.width = 40 + 'px';
            start2.style.width = 40 + 'px';
            relTextPlusTol.innerHTML = Number(binnenPlusTolMm * verdeling).toFixed(0);
            relTextPlusTol2.innerHTML = Number(binnenPlusTolMm * verdeling).toFixed(0);

        }

        function RelOutPlusTol() {
            var start1 = document.getElementById('relOutPlustToleranceText');
            var start2 = document.getElementById('relOutPlustToleranceText2');
            start1.style.width = buitenPlusTolTextRel + 'px';
            start2.style.width = buitenPlusTolTextRel + 'px';

        }

        //Waarschuwing foute tolerantie
        if (binnenPlusTolMm - minTolerantieMm >= bereikKlok) {
            alert("Het bereik van de meetklok is kleiner dan de totale tolerantie");
        } else {
            //maximum breedte
            bars();

            //bovenBar
            RelOutMinTol();
            RelMinTol();
            RelLeegMin();
            RelNul();
            RelLeegPlus();
            RelPlusTol();
            RelOutPlusTol();
            // bar
            OutMinTol();
            InMinTol();
            InPlusTol();
            OutPlusTol();


            //maximum breedte
            bars();

            //bovenBar
            RelOutMinTol();
            RelMinTol();
            RelLeegMin();
            RelNul();
            RelLeegPlus();
            RelPlusTol();
            RelOutPlusTol();
            // bar
            OutMinTol();
            InMinTol();
            InPlusTol();
            OutPlusTol();
        }

        //Info Console
        console.log("maat:                  " + maat + " mm");
        console.log("instellen op           " + instellenOp + " µm");
        console.log("breedte balk:          " + totaleBreedte + " %");
        console.log("breedte balk:          " + breedte + " px")
        console.log("verhouding meetklok:   " + verhouding + " px/mm");
        console.log("verschoven nulpunt:    " + verschovenNulpunt + " mm");
        console.log("minimum tolerantie:    " + minTolerantieMm + " mm");
        console.log("buiten min tol         " + buitenMinTolMm + " mm");
        console.log("buiten min tol         " + buitenMinTolPx + " Px");
        console.log("binnen min tol         " + binnenMinTolMm + " mm");
        console.log("binnen min tol         " + binnenMinTolPx + " px");
        console.log("binnen plus tol        " + binnenPlusTolMm + " mm");
        console.log("binnen plus tol        " + binnenPlusTolPx + " px");
        console.log("buiten plus tol        " + buitenPlusTolMm + " mm");
        console.log("buiten plus tol        " + buitenPlusTolPx + " px");
        console.log("buiten min tol text rel" + buitenMinTolTextRel + " px");
        console.log("nulpunt text           " + nulpuntTextRel + " px");

    });
});

var vanminNaarPlus = 0;

$("#vanPlusNaarMin").hide();

$("#minNaarPlus").on('click', function () {
    $("#vanMinNaarPlus").toggle();
    $("#vanPlusNaarMin").toggle();

    if (vanminNaarPlus === 0){
        var start1 = $("#minNaarPlus");
        start1.val("+ naar -");
        console.log('off');
        vanminNaarPlus = 1;
    }else{
        var start1 = $("#minNaarPlus");
        start1.val("- naar +")  ;
        console.log('on');
        vanminNaarPlus = 0;
    }

});
// $("#plusNaarMin").on('click', function () {
//     $("#vanMinNaarPlus").toggle();
//     $("#vanPlusNaarMin").toggle();
//
// });

// if ($("#vanPlusNaarMin").hide() = true){
//     console.log('hey');
// }
