const omdbBaseURL = "https://www.omdbapi.com/?";
const omdbApiKey = "&apikey=9709cc88";
const omdbIMDBParam = "i=";
const omdbSearchParam = "t=";
const omdbOption1 = "&plot=full";

function isInputEmpty(queryString){
    //Check if the user has inputted something in the input search field (use cleanInput function first to prevent input of ony blank spaces)
    //Return TRUE if empty or only blank spaces, FALSE if valid data was provided
    return (!queryString || 0 === queryString.length);
}

function cleanInput(queryString){
    //Trim leading and trailing spaces and convert to lower case
    return queryString.trim().toLowerCase();
}

function isImdbID(queryString){
    // Function returns TRUE if provide string is has valid IMDB ID format else returns FALSE
    // Expression explanation: check first group equals tt, nm, ch, co, ev or ni and second group is exactly 7 digits if so the string is a valid IMDB ID
    const regex = /^(tt|nm|ch|co|ev|ni)\d{7}$/g;
    var regexp= new RegExp(regex);
    if(regexp.test(queryString)){
        return true;
    } else {
        return false;
    }
}