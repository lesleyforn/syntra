<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Gekke Snake spel!</title>
    </head>

    <body>
        <div class="justify-middle canvas">
            <canvas id="mycanvas" width="350" height="350">
            </canvas>
        </div>
        <div class="justify-middle">
            <h1> Druk op Spelen en eet het eten</h1>
        </div>
        <div class="justify-middle">
            <button id="btn" class="btn">SPELEN</button>
            <button id="focusBtn" class="btn2">Focus Mode</button>
        </div>
        <div>

        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/node.js"></script>
</html>
